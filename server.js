var app = require('express')();
var https = require('https');
var http = require('http');
var fs = require('fs');
var mysql = require('mysql');
var bodyParser = require('body-parser');


// This line is from the Node.js HTTPS documentation.
var options = {
//key: fs.readFileSync('/etc/nginx/ssl/self/millionspaces.key'),
//cert: fs.readFileSync('/etc/nginx/ssl/godaddy/millionspaces.com.chain.crt')
};


var server =  https.createServer(options, app).listen(4000, function () {
    console.log("eventspace app listening at http://%s:%s", 'localhost', server.address().port)
});
var io = require('socket.io').listen(server);
global._IO_SOCKETS = io;

var allowCorsDomain = function(req, res, next) {
    var origin=req.headers.host;
    console.log("origin:"+origin);
    res.setHeader("Access-Control-Allow-Origin","*");
    //res.setHeader("Access-Control-Allow-Credentials", "true");
    res.setHeader("Access-Control-Allow-Methods", "POST, PUT, GET, OPTIONS, DELETE");
    res.setHeader("Access-Control-Max-Age", "3600");
    res.setHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, AUTH-TOKEN");
    next();
}


app.use(allowCorsDomain);

// get our request parameters
app.use(bodyParser.urlencoded({
    extended: false
}));
app.use(bodyParser.json());

// get our request parameters
app.use(bodyParser.urlencoded({
    extended: false
}));
app.use(bodyParser.json());


app.get('/', function (req, res) {
    res.sendfile(__dirname + '/index.html');
    console.log("-index get called");
});

//app.get('/2', function (req, res) {
//    res.sendfile(__dirname + '/index2.html');
//    console.log("-index get called");
//});


app.post('/payment', function (req,res) {

    var successCode = req.body.success_code;
    var id = req.body.user_id;

         if (io.sockets.adapter.rooms[id] != undefined) {
             console.log("user is in online");
            io.to(id).emit('sent_notification', successCode, 1);
            console.log("notification send to user:" + id);
            res.status(200).json({
             status:1
            });
    }
    else{
        console.log("user is not online");
         res.status(200).json({
             status:2
        });
    }
      

});

//to store notifications
var array1 = [];
app.post('/notification', function (req,res) {

    var notification = req.body.notification;
    var id = req.body.user_id;

    //if user is online
    if (io.sockets.adapter.rooms[id] != undefined) {
        io.to(id).emit('sent_notification', notification, 1);
        console.log("notification send to user:" + id);
        res.status(200).json({
        });
        // sendnotification(id);
    }
    else {
        var data = {
            "user_id": id,
            "notification": notification
        };
        array1.push(data);
        res.status(200).json({
        });
        console.log("notification will send when user come online");
    }

});


io.on('connection', function (socket) {
    console.log(socket.id + "-connected");

    //user connected
    socket.on('intiate', function (id) {
        //set same users into same socket room
        socket.join(id);
        console.log("ids " + JSON.stringify(io.sockets.adapter.rooms[id]));
        console.log("connected clients" + io.sockets.server.eio.clientsCount);
        console.log("clients:" + JSON.stringify(io.sockets.adapter.rooms));

        sendnotification(id);

        //send save notifications
        var new_nofitication_count = 0;
        if (array1 != undefined || null) {
            for (var i = 0; i < array1.length; i++) {
                if (array1[i].user_id == id) {
                    new_nofitication_count++;
                    io.to(id).emit('sent_notification', array1[i].notification, new_nofitication_count);
                    console.log("notification send to user:" + id);
                    array1.splice(i, 1);
                    i--;
                }

            }
        }

    });

    //user disconnect
    socket.on('disconnect', function () {
        console.log(socket.id + " is disconnected");
        console.log("clients" + JSON.stringify(io.sockets.adapter.rooms));
    });

});

//send all the notifications from db
function sendnotification(user_id) {
    var connection = mysql.createConnection({
        host: '127.0.0.1',
        user: 'root',
        password: 'aux123',
        database: 'eventspace'
    });

    connection.connect();
    var sql = "select b.id,u1.name as client,u2.name as host,s.name,bh.created_at," +
        " rs.name  as reservation_status,bh.seen,rs.id as res_state_id"+
        " from booking_history  bh,booking  b,space s,user u1,user u2,reservation_status rs" +
        " where u1.id=b.user and u2.id=s.user and b.id=bh.booking and bh.reservation_status=rs.id and" +
        " b.space=s.id and (b.user=? or s.user =?) and bh.action_taker!=? and rs.id!=1 order by bh.created_at";
    connection.query(sql, [user_id, user_id,user_id], function (err, rows, fields) {
        if (!err) {
            console.log('query result is: ' + user_id, rows);
            io.to(user_id).emit('notifications', rows);
        }
        else
            console.log('Error while performing Query.' + err);
    });
    connection.end();
}

